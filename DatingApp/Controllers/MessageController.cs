﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DatingApp.Models;

namespace DatingApp.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        UsersContext db = new UsersContext();

        //
        // GET: /Message/
        public ActionResult Index()
        {
            ViewBag.Message = "See who is messaging you.";

            var name = User.Identity.Name;
            var model = db.UserProfiles.Single(user => user.UserName == name);
            return View(model.Messages);
        }

        // POST: /Message/Create
        [HttpPost]
        public ActionResult Create(string UserName, string Sender, string Heading, string MessageContent)
        {
            var name = User.Identity.Name;
            var user = db.UserProfiles.Single(model => model.UserName == name);
            var recipient = db.UserProfiles.Single(model => model.UserName == UserName);

            Message message = new Message();
            message.SenderId = user.UserId;
            message.UserId = recipient.UserId;
            if (Sender == null || Sender == "")
                message.Sender = user.UserName;//Defaults to username if they don't specify an alias
            else
                message.Sender = Sender;
            message.Heading = Heading;
            message.MessageContent = MessageContent;

            if (ModelState.IsValid)
            {
                db.Messages.Add(message);
                db.SaveChanges();
                return Json(new { Success = true }); // It worked
            }
            return Json(new { Success = false }); // It failed
        }

        //
        // POST:
        [HttpPost]//Old Style: [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int MessageId)
        {
            try
            {
                Message message = db.Messages.Find(MessageId);
                db.Messages.Remove(message);
                db.SaveChanges();
                return Json(new { Success = true }); // It worked
            }
            catch
            {
                return Json(new { Success = false }); // It failed
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [Authorize]
        public ActionResult OpenMessageCreate(string id)
        {
            ViewData["UserName"] = id;
            return PartialView("_MessageCreateDialog");
        }

        [Authorize]
        public ActionResult OpenMessageReply(string id) // this is the same as the OpenMessageCreate ActionResult
            // The reason for the difference is I didn't want the userId shown in the javascript so here the controller is finding it
        {
            Message message = db.Messages.Find(Convert.ToInt32(id)); // find the message in the database (Have to convert url string to int)

            if (message == null)
            {
                return HttpNotFound();
            }

            UserProfile Recipient = db.UserProfiles.Find(message.SenderId); // Get the Sender from the database

            //Future Note: To add old message data to reply Im thinking passing a ViewData object

            ViewData["UserName"] = Recipient.UserName;
            return PartialView("_MessageCreateDialog");
        }

        public ActionResult OpenMessageDetails(string id)
        {
            Message message = db.Messages.Find(Convert.ToInt32(id)); // find the message in the database (Have to convert url string to int)

            if (message == null)
            {
                return HttpNotFound();
            }

            ViewData["Message"] = message; //I need the whole message for the Mesage Details 

            return PartialView("_MessageDetailsDialog");
        }

        public ActionResult OpenMessageDelete(string id)
        {
            Message message = db.Messages.Find(Convert.ToInt32(id)); // find the message in the database (Have to convert url string to int)

            if (message == null)
            {
                return HttpNotFound();
            }

            ViewData["Message"] = message; //I need the whole message for the Mesage Details 

            return PartialView("_MessageDeleteDialog");
        }
    }
}
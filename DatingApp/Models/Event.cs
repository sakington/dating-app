﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DatingApp.Models
{
    public class Event
    {
        public class Skeleton
        {
            //EventId for Primary Key 
            [Required]
            [ScaffoldColumn(false)]
            public long EventId { get; set; }

            //UserId for Foreign Key 
            [Required]
            [ScaffoldColumn(false)]
            public long UserId { get; set; }

            //Event Title (Headline)
            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Event Title")]
            public string Title { get; set; }

            //Event Start Date
            [Required]
            [DataType(DataType.DateTime)]
            [Display(Name = "Starting Date and Time")]
            [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
            public DateTime StartDate { get; set; }

            //Event Duration
            [Required]
            [DataType(DataType.Duration)]
            [Display(Name = "Length of Event")]
            public DateTime Duration { get; set; }

            //Event Description (Story) 
            [Required]
            [DataType(DataType.MultilineText)]
            [Display(Name = "Event Description")]
            public string Description { get; set; }

            //Attending User Count 
            [Range(1, Int32.MaxValue)]
            [Display(Name = "Users Currently Attending")]
            [ScaffoldColumn(false)]
            public int UserCount { get; set; }

            //Pictures a One to Many
            //I made this a collection as Users may want to share more then one picture for an event
            public virtual Collection<Image> Images { get; private set; }
        }
    }
}
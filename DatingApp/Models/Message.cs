﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DatingApp.Models
{
    public class Message
    {
        //Message Id as Primary Key
        [Required]
        [ScaffoldColumn(false)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]//here
        public int MessageId { get; set; }

        //UserId for Foreign Key (Recipient) 
        [Required]
        [ScaffoldColumn(false)]
        public int UserId { get; set; }

        //SenderId for Foreign Key (Sender) 
        [Required]
        [ScaffoldColumn(false)]
        public int SenderId { get; set; }

        //Sender Name (I Did this for people wishing to do anonymous messages)
        [Required] 
        [DataType(DataType.Text)]
        [Display(Name = "From")]
        public string Sender { get; set; }

        //Heading of Message
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Heading")]
        public string Heading { get; set; }

        //Content of Message
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Message")]
        public string MessageContent{ get; set; }

        //Pictures a One to Many
        //I made this a collection as Users may want to send more then one picture to others in one message
        public virtual Collection<Image> Images { get; private set; }//here
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace DatingApp.Models
{
    public class CustomValidation
    {
        public class EighteenYearsOrOlder : ValidationAttribute
        {
            public override bool IsValid(object value)
            {
                DateTime BirthDate = (DateTime)value;
                DateTime Today = DateTime.Today;

                TimeSpan Age = Today.Subtract(BirthDate); // Gets exact age of user
                Double Year = Age.TotalDays / 365; // moves the years above the decimal

                if (Year >= 18)
                    return true;
                else
                    return false;
            }
        }

        public class ValidPostalCode : ValidationAttribute
        {
            public override bool IsValid(object value)
            {
                String code = value.ToString();

                if (code.Length >= 3 && code.Length <= 10)//If code is between 3 and 10 characters
                {
                    if (Regex.IsMatch(code, @"^[a-zA-Z0-9]+$"))//And alphanumeric 
                        return true;

                    else//if not alphanumeric
                        return false;
                }

                else//if not between 3 and 10 characters
                {
                    return false;
                }
            }
        }
    }
}